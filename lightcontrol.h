#ifndef LIGHTCONTROL_H
#define LIGHTCONTROL_H

#include <QDialog>

namespace Ui {
class LightControl;
}

class LightControl : public QDialog
{
    Q_OBJECT

public:
    explicit LightControl(QWidget *parent = nullptr);
    ~LightControl();

private:
    Ui::LightControl *ui;
};

#endif // LIGHTCONTROL_H

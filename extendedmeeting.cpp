﻿#include "extendedmeeting.h"
#include "ui_extendedmeeting.h"

ExtendedMeeting::ExtendedMeeting(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExtendedMeeting)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(0.8);
    this->setWindowFlags(Qt::FramelessWindowHint);

    connect(ui->btn_yes, &QPushButton::clicked, this, &QDialog::hide);
    connect(ui->btn_delete, &QPushButton::clicked, this, &QDialog::hide);
}

ExtendedMeeting::~ExtendedMeeting()
{
    delete ui;
}

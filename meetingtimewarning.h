#ifndef MEETINGTIMEWARNING_H
#define MEETINGTIMEWARNING_H

#include <QDialog>

namespace Ui {
class MeetingTimeWarning;
}

class MeetingTimeWarning : public QDialog
{
    Q_OBJECT

public:
    explicit MeetingTimeWarning(QWidget *parent = nullptr);
    ~MeetingTimeWarning();

private:
    Ui::MeetingTimeWarning *ui;
};

#endif // MEETINGTIMEWARNING_H

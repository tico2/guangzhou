﻿#ifndef TOOL_H
#define TOOL_H

#include "mainwindow.h"
#include "meetingtimewarning.h"

#include <QDebug>
#include <QMainWindow>
#include <QObject>

class Tool
{
public:
    static Tool* GetInstance();
private:
    //mainwindow
    MainWindow *mainwindow;
    LocalMeeting *localmeeting;
    VoiceConference *voiceconference;
    OpenSystem *opensystem;
    SpaceUsage1 *spaceusage;
    //dialog
    CurtainControl *curtaincontrol;
    ExtendedMeeting *extendedmeeting;
    LightControl *lightcontrol;
    LocalMeetingConfirm *localmeetingconfirm;
    MeetingTimeWarning *meetingtimewarning;
    PageChange *pagechange;
    VoiceControl *voicecontrol;

    Tool(MainWindow *mainwindow, LocalMeeting *localmeeting, VoiceConference *voiceconference, OpenSystem *opensystem,
    SpaceUsage1 *spaceusage, CurtainControl *curtaincontrol, ExtendedMeeting *extendedmeeting, LightControl *lightcontrol,
    LocalMeetingConfirm *localmeetingconfirm, MeetingTimeWarning *meetingtimewarning, PageChange *pagechange, VoiceControl *voicecontrol){

        this->mainwindow = mainwindow;
        this->localmeeting = localmeeting;
        this->voiceconference = voiceconference;
        this->opensystem = opensystem;
        this->spaceusage = spaceusage;
        this->curtaincontrol = curtaincontrol;
        this->extendedmeeting = extendedmeeting;
        this->lightcontrol = lightcontrol;
        this->localmeetingconfirm = localmeetingconfirm;
        this->meetingtimewarning = meetingtimewarning;
        this->pagechange = pagechange;
        this->voicecontrol =voicecontrol;

    }
//    void windowSwitch(int btnId);
private:
    static Tool *m_pTool;

    class GC{
    public:
        ~GC(){
            if(m_pTool != nullptr){
                qDebug("Here destroy the m_pTool...");
                delete m_pTool;
                m_pTool = nullptr;
            }
        }
        static GC gc;
    };
};


#endif // TOOL_H

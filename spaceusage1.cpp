﻿#include "spaceusage1.h"
#include "tool.h"
#include "ui_spaceusage1.h"

#include <QStyle>
#include <QDesktopWidget>

SpaceUsage1::SpaceUsage1(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SpaceUsage1)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(1);
    this->setWindowFlags(Qt::FramelessWindowHint);
//    extendedmeeting = new ExtendedMeeting(this);
//    extendedmeeting->hide();
    btngroupcurtain = new QButtonGroup(this);
    btngroupcurtain->addButton(ui->btn_curtain_full_open, 1);
    btngroupcurtain->addButton(ui->btn_curtain_half_open, 2);
    btngroupcurtain->addButton(ui->btn_curtain_three_quarter_open, 3);
    btngroupcurtain->addButton(ui->btn_curtain_full_close, 4);
    btngroupcurtain->addButton(ui->btn_curtain_stairs,5);

//    connect(ui->btn_extended_meeting, &QPushButton::clicked, extendedmeeting, &QDialog::show);
    connect(ui->btn_conclude_meeting,&QPushButton::clicked, this, &QMainWindow::hide);
    connect(btngroupcurtain, QOverload<int>::of(&QButtonGroup::buttonClicked), this, &SpaceUsage1::curtainBgChange);

    //延长会议时间
    connect(ui->btn_extended_meeting, &QPushButton::clicked, this, [=]{
        Tool::windowSwitch(9);

    });

    //系统开启
    connect(ui->btn_system_open1, &QPushButton::clicked, this, [=]{
        Tool::windowSwitch(4);
    });
}

SpaceUsage1::~SpaceUsage1()
{
    delete ui;
    btngroupcurtain->deleteLater();
}

void SpaceUsage1::curtainBgChange(int buttonID){
    switch (buttonID) {
    case 1:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg1.png);");
        break;
    case 2:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg2.png);");
        break;
    case 3:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg3.png);");
        break;
    case 4:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg4.png);");
        break;
    case 5:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg5.png);");
        break;
    }
}

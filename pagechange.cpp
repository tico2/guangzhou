﻿#include "pagechange.h"
#include "ui_pagechange.h"

#include <QStyle>
#include <QDesktopWidget>
#include <QBitmap>
#include <QPainter>

PageChange::PageChange(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PageChange)
{
    ui->setupUi(this);
    //页面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(0.8);
    this->setWindowFlags(Qt::FramelessWindowHint);
    connect(ui->btn_delete, &QPushButton::clicked, this, &QDialog::hide);
}

PageChange::~PageChange()
{
    delete ui;
}

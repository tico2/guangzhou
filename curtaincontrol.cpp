﻿#include "curtaincontrol.h"
#include "ui_curtaincontrol.h"

#include <QtGlobal>
#include <QButtonGroup>

CurtainControl::CurtainControl(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CurtainControl)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(0.8);
    this->setWindowFlags(Qt::FramelessWindowHint);
    btngroupcurtain = new QButtonGroup(this);
    btngroupcurtain->addButton(ui->btn_curtain_full_open, 1);
    btngroupcurtain->addButton(ui->btn_curtain_half_open, 2);
    btngroupcurtain->addButton(ui->btn_curtain_three_quarter_open, 3);
    btngroupcurtain->addButton(ui->btn_curtain_full_close, 4);
    btngroupcurtain->addButton(ui->btn_curtain_stairs,5);

    connect(ui->btn_delete, &QPushButton::clicked, this, &QDialog::hide);
    connect(btngroupcurtain, QOverload<int>::of(&QButtonGroup::buttonClicked), this, &CurtainControl::curtainBgChange);

}

CurtainControl::~CurtainControl()
{
    delete ui;
    btngroupcurtain->deleteLater();
}

void CurtainControl::curtainBgChange(int buttonID){
    switch (buttonID) {
    case 1:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg11.png);");
        //ui->btn_curtain_full_open->
        break;
    case 2:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg22.png);");
        break;
    case 3:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg33.png);");
        break;
    case 4:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg44.png);");
        break;
    case 5:
        ui->frame_curtain_bg->setStyleSheet("border-image: url(:/images/res/curtain_bg55.png);");
        break;
    }
}

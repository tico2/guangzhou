QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    curtaincontrol.cpp \
    extendedmeeting.cpp \
    lightcontrol.cpp \
    localmeeting.cpp \
    localmeetingconfirm.cpp \
    main.cpp \
    mainwindow.cpp \
    meetingtimewarning.cpp \
    opensystem.cpp \
    pagechange.cpp \
    spaceusage1.cpp \
    tool.cpp \
    voiceconference.cpp \
    voicecontrol.cpp

HEADERS += \
    curtaincontrol.h \
    extendedmeeting.h \
    lightcontrol.h \
    localmeeting.h \
    localmeetingconfirm.h \
    mainwindow.h \
    meetingtimewarning.h \
    opensystem.h \
    pagechange.h \
    spaceusage1.h \
    tool.h \
    voiceconference.h \
    voicecontrol.h

FORMS += \
    curtaincontrol.ui \
    extendedmeeting.ui \
    lightcontrol.ui \
    localmeeting.ui \
    localmeetingconfirm.ui \
    mainwindow.ui \
    meetingtimewarning.ui \
    opensystem.ui \
    pagechange.ui \
    spaceusage1.ui \
    voiceconference.ui \
    voicecontrol.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

﻿#include "voicecontrol.h"
#include "ui_voicecontrol.h"

VoiceControl::VoiceControl(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VoiceControl)
{
    ui->setupUi(this);
    //页面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(0.8);
    this->setWindowFlags(Qt::FramelessWindowHint);
    connect(ui->btn_delete, &QPushButton::clicked, this, &QDialog::close);
}

VoiceControl::~VoiceControl()
{
    delete ui;
}

﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "opensystem.h"
#include "spaceusage1.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void iniUI();

private:
    Ui::MainWindow *ui;


//private slots:
    //void cleanModeOption(bool press);


};


#endif // MAINWINDOW_H

#ifndef VOICECONTROL_H
#define VOICECONTROL_H

#include <QDialog>

namespace Ui {
class VoiceControl;
}

class VoiceControl : public QDialog
{
    Q_OBJECT

public:
    explicit VoiceControl(QWidget *parent = nullptr);
    ~VoiceControl();

private:
    Ui::VoiceControl *ui;
};

#endif // VOICECONTROL_H

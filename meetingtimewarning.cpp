﻿#include "meetingtimewarning.h"
#include "ui_meetingtimewarning.h"

MeetingTimeWarning::MeetingTimeWarning(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MeetingTimeWarning)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(0.8);
    this->setWindowFlags(Qt::FramelessWindowHint);

    connect(ui->btn_yes , &QPushButton::clicked, this, &QDialog::accept);
    connect(ui->btn_no , &QPushButton::clicked, this, &QDialog::reject);
}

MeetingTimeWarning::~MeetingTimeWarning()
{
    delete ui;
}

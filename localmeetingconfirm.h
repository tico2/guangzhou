﻿#ifndef LOCALMEETINGCONFIRM_H
#define LOCALMEETINGCONFIRM_H

#include "localmeeting.h"

#include <QDialog>

namespace Ui {
class LocalMeetingConfirm;
}

class LocalMeetingConfirm : public QDialog
{
    Q_OBJECT

public:
    explicit LocalMeetingConfirm(QWidget *parent = nullptr);
    ~LocalMeetingConfirm();

private:
    Ui::LocalMeetingConfirm *ui;
//    LocalMeeting *localmeeting;

};

#endif // LOCALMEETINGCONFIRM_H

#ifndef PAGECHANGE_H
#define PAGECHANGE_H

#include <QDialog>

namespace Ui {
class PageChange;
}

class PageChange : public QDialog
{
    Q_OBJECT

public:
    explicit PageChange(QWidget *parent = nullptr);
    ~PageChange();

private:
    Ui::PageChange *ui;
};

#endif // PAGECHANGE_H

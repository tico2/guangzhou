﻿#include "lightcontrol.h"
#include "ui_lightcontrol.h"

LightControl::LightControl(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LightControl)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(0.8);
    this->setWindowFlags(Qt::FramelessWindowHint);
    connect(ui->btn_delete, &QPushButton::clicked, this, &QDialog::close);
}

LightControl::~LightControl()
{
    delete ui;
}

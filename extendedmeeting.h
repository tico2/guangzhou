﻿#ifndef EXTENDEDMEETING_H
#define EXTENDEDMEETING_H

#include <QDialog>

namespace Ui {
class ExtendedMeeting;
}

class ExtendedMeeting : public QDialog
{
    Q_OBJECT

public:
    explicit ExtendedMeeting(QWidget *parent = nullptr);
    ~ExtendedMeeting();

private:
    Ui::ExtendedMeeting *ui;

};

#endif // EXTENDEDMEETING_H

﻿#include "voiceconference.h"
#include "ui_voiceconference.h"

VoiceConference::VoiceConference(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VoiceConference)
{
    ui->setupUi(this);
    //页面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(1);
    this->setWindowFlags(Qt::FramelessWindowHint);

//    extendedmeeting = new ExtendedMeeting(this);
//    extendedmeeting->hide();
//    voicecontrol = new VoiceControl(this);
//    voicecontrol->hide();

    connect(ui->btn_conclude_meeting,&QPushButton::clicked, this, &QMainWindow::hide);
//    connect(ui->btn_extended_meeting, &QPushButton::clicked, extendedmeeting, &QDialog::show);
//    connect(ui->btn_voice_control, &QPushButton::clicked, voicecontrol, &QDialog::show);
}

VoiceConference::~VoiceConference()
{
    delete ui;
}

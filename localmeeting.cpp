﻿#include "localmeeting.h"
#include "tool.h"
#include "ui_localmeeting.h"

LocalMeeting::LocalMeeting(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LocalMeeting)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(1);
    this->setWindowFlags(Qt::FramelessWindowHint);
    //    extendedmeeting = new ExtendedMeeting(this);
    //    extendedmeeting->hide();
    //    pagechange = new PageChange(this);
    //    pagechange->hide();
    //    voicecontrol = new VoiceControl(this);
    //    voicecontrol->hide();
    //    curtaincontrol = new CurtainControl(this);
    //    curtaincontrol->hide();
    //    lightcontrol = new LightControl(this);
    //    lightcontrol->hide();
    connect(ui->btn_conclude_meeting,&QPushButton::clicked, this, &QMainWindow::close);
    connect(ui->btn_extended_meeting, &QPushButton::clicked, this , [=]{
        Tool::windowSwitch(9);
    });
//    connect(ui->btn_voice_control, &QPushButton::clicked, voicecontrol, &QDialog::show);
//    connect(ui->btn_curtain_control, &QPushButton::clicked, curtaincontrol, &QDialog::show);
//    connect(ui->btn_light_control, &QPushButton::clicked, lightcontrol, &QDialog::show);
    //connect(ui->btn_page_change,&QPushButton::clicked, pagechange, );
}

LocalMeeting::~LocalMeeting()
{
    delete ui;
}

﻿#include "localmeetingconfirm.h"
#include "ui_localmeetingconfirm.h"

LocalMeetingConfirm::LocalMeetingConfirm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LocalMeetingConfirm)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(0.8);
    this->setWindowFlags(Qt::FramelessWindowHint);

//    localmeeting = new LocalMeeting(this);
//    localmeeting->hide();
//    connect(ui->btn_yes , &QPushButton::clicked, localmeeting, &QMainWindow::show);
    connect(ui->btn_yes , &QPushButton::clicked, this, &QDialog::accept);
    connect(ui->btn_no , &QPushButton::clicked, this, &QDialog::reject);
}

LocalMeetingConfirm::~LocalMeetingConfirm()
{
    delete ui;
}

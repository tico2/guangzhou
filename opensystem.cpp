﻿#include "opensystem.h"
#include "tool.h"
#include "ui_opensystem.h"

OpenSystem::OpenSystem(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::OpenSystem)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(1);
    this->setWindowFlags(Qt::FramelessWindowHint);

//    localmeeting = new LocalMeeting(this);
//    localmeetingconfirm = new LocalMeetingConfirm(this);
//    localmeetingconfirm->hide();

//    connect(ui->btn_local_meeting, &QPushButton::clicked, localmeetingconfirm, [=](bool confirm) {
//        if (localmeetingconfirm->exec()) {
//            // 开启本地会议

//        }
//    });
    connect(ui->btn_return, &QPushButton::clicked, this, &QMainWindow::hide);

    //本地会议确认弹窗
    connect(ui->btn_local_meeting, &QPushButton::clicked, this, [=]{
        Tool::windowSwitch(8);
    });

    //视频会议选择弹窗（MaxHub或者个人PC）
    connect(ui->btn_video_meeting, &QPushButton::clicked, this, [=]{
        Tool::windowSwitch(7);
    });

    //语音会议页面
    connect(ui->btn_voice_meeting, &QPushButton::clicked, this, [=]{
        Tool::windowSwitch(3);
    });
}
OpenSystem::~OpenSystem()
{
    delete ui;
}

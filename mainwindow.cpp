﻿#include "mainwindow.h"
#include "tool.h"
#include "ui_mainwindow.h"

#include <QStyle>
#include<QDesktopWidget>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //界面居中、去除工具栏
    this->setGeometry(0, 0, 1800, 1200);
    this->setWindowOpacity(1);
    this->setWindowFlags(Qt::FramelessWindowHint);

//    connect(ui->btn_cleaning_mode, &QPushButton::pressed, this, [=](bool pressed){
//        if(pressed){
//            //清洁模式打开
//            qDebug("CleanMode on");

//        }else{
//            //清洁模式关闭
//            qDebug("CleanMode off");

//        }
//    });

    connect(ui->btn_space_usage, &QPushButton::clicked, this, [=]{
        Tool::windowSwitch(5);

    });

    connect(ui->btn_open_system, &QPushButton::clicked, this, [=]{
        Tool::windowSwitch(4);
    });

//    spaceusage = new SpaceUsage1(this);
//    spaceusage->hide();
//    opensystem = new OpenSystem(this);
//    opensystem->hide();
//    connect(ui->btn_space_usage, &QPushButton::clicked, spaceusage, &QMainWindow::show);
//    connect(ui->btn_open_system, &QPushButton::clicked, opensystem, &QMainWindow::show);
//    connect(ui->btn_cleaning_mode, &QPushButton::pressed, ui->btn_cleaning_mode, cleanModeOption);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::iniUI(){

}

//void MainWindow::cleanModeOption(bool press){

//}

//PagesInitlize::PagesInitlize(QWidget *parent) : QMainWindow(parent){
//    mainwindow = new MainWindow();
//    localmeeting = new LocalMeeting();
//    voiceconference = new VoiceConference();
//    opensystem = new OpenSystem();
//    spaceusage = new SpaceUsage1();
//}


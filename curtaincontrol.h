﻿#ifndef CURTAINCONTROL_H
#define CURTAINCONTROL_H

#include <QButtonGroup>
#include <QDialog>

namespace Ui {
class CurtainControl;
}

class CurtainControl : public QDialog
{
    Q_OBJECT

public:
    explicit CurtainControl(QWidget *parent = nullptr);
    ~CurtainControl();

private:
    Ui::CurtainControl *ui;
    QButtonGroup *btngroupcurtain;

private slots:
    void curtainBgChange(int buttonID);
};

#endif // CURTAINCONTROL_H

﻿#ifndef OPENSYSTEM_H
#define OPENSYSTEM_H


#include "localmeetingconfirm.h"
#include "pagechange.h"
#include "voiceconference.h"

#include <QMainWindow>

namespace Ui {
class OpenSystem;
}

class OpenSystem : public QMainWindow
{
    Q_OBJECT

public:
    explicit OpenSystem(QWidget *parent = nullptr);
    ~OpenSystem();

private:
    Ui::OpenSystem *ui;

    //LocalMeetingConfirm *localmeetingconfirm;
    //LocalMeeting *localmeeting;
    //VoiceConference *voiceconference;
    //PageChange *pagechange;
};

#endif // OPENSYSTEM_H

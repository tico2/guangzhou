﻿#ifndef LOCALMEETING_H
#define LOCALMEETING_H

#include "curtaincontrol.h"
#include "extendedmeeting.h"
#include "lightcontrol.h"
#include "pagechange.h"
#include "voicecontrol.h"

#include <QMainWindow>

namespace Ui {
class LocalMeeting;
}

class LocalMeeting : public QMainWindow
{
    Q_OBJECT

public:
    explicit LocalMeeting(QWidget *parent = nullptr);
    ~LocalMeeting();

private:
    Ui::LocalMeeting *ui;

//    ExtendedMeeting *extendedmeeting;
//    PageChange *pagechange;
//    VoiceControl *voicecontrol;
//    CurtainControl *curtaincontrol;
//    LightControl *lightcontrol;
};

#endif // LOCALMEETING_H

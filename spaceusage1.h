﻿#ifndef SPACEUSAGE1_H
#define SPACEUSAGE1_H

#include "extendedmeeting.h"

#include <QButtonGroup>
#include <QMainWindow>

namespace Ui {
class SpaceUsage1;
}

class SpaceUsage1 : public QMainWindow
{
    Q_OBJECT

public:
    explicit SpaceUsage1(QWidget *parent = nullptr);
    ~SpaceUsage1();

private:
    Ui::SpaceUsage1 *ui;
    QButtonGroup *btngroupcurtain;

//    ExtendedMeeting *extendedmeeting;

private slots:
    void curtainBgChange(int buttonID);
};

#endif // SPACEUSAGE1_H

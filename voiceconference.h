﻿#ifndef VOICECONFERENCE_H
#define VOICECONFERENCE_H

#include "extendedmeeting.h"
#include "voicecontrol.h"

#include <QMainWindow>

namespace Ui {
class VoiceConference;
}

class VoiceConference : public QMainWindow
{
    Q_OBJECT

public:
    explicit VoiceConference(QWidget *parent = nullptr);
    ~VoiceConference();

private:
    Ui::VoiceConference *ui;

//    ExtendedMeeting *extendedmeeting;
//    VoiceControl *voicecontrol;
};

#endif // VOICECONFERENCE_H
